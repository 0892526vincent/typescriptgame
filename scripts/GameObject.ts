class GameObject {

    private _x: number = 0;
    private _y: number = 0;
    private readonly _width: number = 0;
    private readonly _height: number = 0;
    private _rotation: number = 0;
    private readonly _div: HTMLElement;

    // get and set x
    public get x(): number {
        return this._x
    };

    public set x(value: number) {
        this._x = value
    };

    // get and set y
    public get y(): number {
        return this._y
    };

    public set y(val: number) {
        this._y = val
    };

    // get width
    public get width(): number {
        return this._width
    };

    // get height
    public get height(): number {
        return this._height
    };

    // get and set rotation
    public get rotation(): number {
        return this._rotation
    };

    public set rotation(val: number) {
        this._rotation = val
    };

    // get div
    public get div(): HTMLElement {
        return this._div
    };

    constructor(x: number, y: number, rotation: number, tag: string) {
        this._x = x;
        this._y = y;
        this._rotation = rotation;

        this._div = document.createElement(tag);
        document.body.appendChild(this._div);

        this._width = this._div.clientWidth;
        this._height = this._div.clientHeight;

        this.draw();
    }

    public collides(gameObject: GameObject): boolean {
        return (this.x < gameObject.x + gameObject.width &&
            this.x + this.width > gameObject.x &&
            this.y < gameObject.y + gameObject.height &&
            this.y + this.height > gameObject.y);
    }

    public remove(gameObject: GameObject, array: Array<any>) {
        gameObject.div.remove();

        let i: number = array.indexOf(gameObject);
        if (i != -1) {
            array.splice(i, 1);
        }
    }

    public update(): void {}

    public draw(): void {
        this._div.style.transform = "translate(" + this.x + "px, " + this.y + "px) rotate(" + this.rotation + "deg)";
    }
}
