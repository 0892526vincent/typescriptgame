class Game {

    private _player: Array<Player> = [];
    private _enemies: Array<Enemy> = [];
    private _drugs: Array<GameObject> = [];
    private _nukes: Array<Nuke> = [];
    private _nuke: Nuke;

    private static instance: Game;

    private constructor() {
        let player = new Player();
        this._player.push(player);

        let drugs = new Drugs();
        this._drugs.push(drugs);

        this._nuke = new Nuke();
        this._nukes.push(this._nuke);

        for (let i = 0; i < 5; i++) {
            let enemy = new Enemy(this._nuke, this._enemies);
            this._enemies.push(enemy);
        }

        requestAnimationFrame(() => this.gameObjectsLoop());
    }

    public static getInstance() {
        if (!Game.instance) {
            Game.instance = new Game()
        }
        return Game.instance
    }


    private gameObjectsLoop() {

        if (this._enemies.length > 0 && this._player.length > 0) {
            for (let player of this._player) {
                for (let enemy of this._enemies) {
                    let collision = player.collides(enemy);
                    if (collision) {
                        player.remove(player, this._player);
                    }
                }

                for (let powerUp of this._drugs) {
                    let collision = player.collides(powerUp);
                    if (collision) {
                        player.fireBehaviour = new FireSuperBomb(player);
                        powerUp.remove(powerUp, this._drugs);
                    }
                }

                for (let nuke of this._nukes) {
                    let collision = player.collides(nuke);
                    if (collision) {
                        nuke.activate(this._nukes);
                    }
                }

                for (let bullet of player.bulletList) {
                    for (let enemy of this._enemies) {
                        let collision = bullet.collides(enemy);
                        if (collision) {
                            enemy.remove(enemy, this._enemies);
                            bullet.remove(bullet, player.bulletList);
                        }
                    }
                    bullet.draw();
                    bullet.update();
                }

                player.update();
                player.draw();
            }

            for (let enemy of this._enemies) {
                enemy.draw();
                enemy.update();
            }
            // only one instance of the keyinputs, singleton
            KeyInput.getInstance().inputLoop();
            requestAnimationFrame(() => this.gameObjectsLoop());

        } else {
            new Message('message', 'restart the game')
        }
    }
}
