class Drugs extends GameObject {

    constructor() {
        // create drugs object
        super(
            Math.floor((Math.random() * window.innerWidth) + 1) - 0.5,
            Math.floor((Math.random() * window.innerHeight) + 1) - 0.5,
            0,
            'drugs'
        );
    }

    public activate(player:Player) : IFireBehaviour {
        return new FireSuperBomb(player);
    }
}
