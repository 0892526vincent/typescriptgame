class Player extends GameObject {

    private _angle: number = 5;
    private _fireBehaviour: IFireBehaviour;
    private _speedLimit: number = 5;
    private _bulletList: Array<Bullet> = [];

    public set fireBehaviour(fireBehaviour : IFireBehaviour) {this._fireBehaviour = fireBehaviour};
    public get bulletList() : Array<Bullet> {return this._bulletList};

    constructor() {
        // create the player
        super(
            window.innerWidth / 2,
            window.innerHeight / 2,
            0,
            'player'
        );

        this._fireBehaviour = new FireBullet(this);

        // left key
        KeyInput.getInstance().addKeycodeCallback(37, () => {
            this.turn(-this._angle);
        });
        // right key
        KeyInput.getInstance().addKeycodeCallback(39, () => {
            this.turn(+this._angle);
        });
        // up key
        KeyInput.getInstance().addKeycodeCallback(38, () => {
            this.moveForward();
        });
        // space bar
        KeyInput.getInstance().addKeycodeCallback(32, () => {
            this._fireBehaviour.fire();
        });
    }

    private moveForward() {
        this.x += this._speedLimit * Math.cos(this.rotation * Math.PI / 180);
        this.y += this._speedLimit * Math.sin(this.rotation * Math.PI / 180);
    }

    public move() {
        if (this.x + this.div.clientWidth < 0) {
            this.x = window.innerWidth;
        }
        if (this.x > window.innerWidth) {
            this.x = 0 - this.div.clientWidth;
        }

        if (this.y + this.div.clientHeight < 0) {
            this.y = window.innerHeight;
        }
        if (this.y > window.innerHeight) {
            this.y = 0 - this.div.clientHeight;
        }
    }

    public turn(angle: number) {
        this.rotation += angle;
    }

    // fire on update
    public update(): void {
        this._fireBehaviour.fireLimit();
        this.move();
    }
}
