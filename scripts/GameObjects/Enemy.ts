class Enemy extends GameObject implements Observer {
    private _speedX : number = Math.random() * 1.8;
    private _speedY : number = Math.random() * 1.8;

    private readonly _enemyList : Array<Enemy>;
    private _subject : Subject;

    constructor(subject:Subject, array:Array<Enemy>) {
        super(
            Math.floor((Math.random() * window.innerWidth) + 1),
            Math.floor((Math.random() * window.innerHeight) + 1),
            0,
            'enemy'
        );

        this._enemyList = array;
        this._subject = subject;

        subject.subscribe(this);
    }

    private move() {
        if (this.x + this.div.clientWidth < 0) {
            this.x = window.innerWidth;
        }

        if (this.x > window.innerWidth) {
            this.x = 0 - this.div.clientWidth;
        }

        if (this.y + this.div.clientHeight < 0) {
            this.y = window.innerHeight;
        }

        if (this.y > window.innerHeight) {
            this.y = 0 - this.div.clientHeight;
        }
    }

    public update(): void {
        this.x += this._speedX;
        this.y += this._speedY;
        this.move();
    }

    public notify() {
        this.remove(this, this._enemyList);
    }

    public remove(gameObject:GameObject, array:Array<any>) {
        gameObject.div.remove();
        this._subject.unsubscribe(this);

        let i:number = array.indexOf(gameObject);
        if(i != -1) {
            array.splice(i, 1);
        }
    }
}
