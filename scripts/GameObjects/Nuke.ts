class Nuke extends GameObject implements Subject {

    public observers : Array<Observer> = [];

    constructor() {
        // create nuke object
        super(
            Math.floor((Math.random() * window.innerWidth) + 1) - 0.5,
            Math.floor((Math.random() * window.innerHeight) + 1) - 0.5,
            0,
            'nuke');
    }

    subscribe(observer: Observer): void {
        this.observers.push(observer);
    }

    unsubscribe(observer: Observer): void {
        let i:number = this.observers.indexOf(observer);
        if(i != -1) {
            this.observers.splice(i, 1);
        }
    }

    public activate(array: Array<Nuke>): void {
        for (let o of this.observers) {
            o.notify();
        }
        // remove nuke after collision with Player
        this.div.remove();
    }
}
