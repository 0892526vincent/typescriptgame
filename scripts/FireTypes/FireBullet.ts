///<reference path="../IFireBehaviour/FireType.ts"/>
class FireBullet extends FireType {
    public fire(): void {
        if (this.fireLimiter > 0) {
            return;
        }
        this.player.bulletList.push(new Bullet(this.player.x + 20, this.player.y + 25, this.player.rotation, this.player.bulletList, 'bullet'));

        this.fireLimiter = 10;
    }
}
