///<reference path="../IFireBehaviour/FireType.ts"/>
class FireSuperBomb extends FireType {
    public fire(): void {
        if (this.fireLimiter > 0) {
            return;
        }
        this.player.bulletList.push(new Bullet(this.player.x + 10, this.player.y + 25, this.player.rotation, this.player.bulletList, 'bullet'));
        this.player.bulletList.push(new Bullet(this.player.x + 10, this.player.y + 25, this.player.rotation + 10, this.player.bulletList, 'bullet'));
        this.player.bulletList.push(new Bullet(this.player.x + 10, this.player.y + 25, this.player.rotation - 10, this.player.bulletList, 'bullet'));

        this.fireLimiter = 25;
    }
}
