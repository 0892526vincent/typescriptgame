interface Subject {
    observers: Array<Observer>;
    subscribe (observer:Observer): void;
    unsubscribe (observer:Observer): void;
}
