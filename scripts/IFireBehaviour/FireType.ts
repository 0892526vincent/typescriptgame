abstract class FireType implements IFireBehaviour {
    private _fireLimiter : number = 0;
    private _player : Player;

    public get fireLimiter(): number {
        return this._fireLimiter
    };
    public set fireLimiter(value: number) {
        this._fireLimiter = value;
    };

    public get player(): Player {
        return this._player;
    }

    constructor(player:Player) {
        this._player = player;
    }

    public fireLimit() : void {
        if (this._fireLimiter > 0) {
            this._fireLimiter -= 0.5;
        }
    }

    public fire(): void {

    }
}
