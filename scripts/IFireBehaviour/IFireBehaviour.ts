interface IFireBehaviour {
    fire() : void;
    fireLimit() : void;
}
