# TypescriptGame

## Getting started
`$ git clone` this repository

`$ npm install` to install all packages listed in package.json

`$ npm run gulp` to run the project

## Controls
Use the arrow keys to control the boat.

Use the space bar to fire at the policeboats.

Pick up the drugs package to fire a superbomb.

Pick up the Nuke to destory all policeboats at once and win!

## Design Patterns

### Singleton
I used the Singleton pattern within Game.ts, because there is only one Game. The Game class is responsible for all the GameObject instances.
This Singleton is used to draw and update all GameObjects.

### Strategy
I used the Strategy pattern within Player.ts, because the player (the drugsboat) has different firing behaviours. Once it picks up drugs, the firing behaviour changes to FireSuperBomb.

### Observer
I used the Observer pattern with Enemy.ts (as observer) and Nuke.ts (as subject). This pattern is useful here, because every enemy should be notified (killed) if the nuke is taken by the player.

### Polymorphism
I used Polymorphism to check whether a Player, Enemy or Drugs collide. I did this by using a GameObject class. Every GameObject extends that class. Therefore, all collision checks can be done from the Game.
Furthermore, I used Polymorphism to seperate the logic from FireBullet and FireSuperBomb where it was the same. I created an abstract FireType class which implements the IFireBehaviour interface.

## UML
![uml](UML.png)

## Links

#### pull request:
https://github.com/DennievanderStarre/PRG01-8/pull/2

#### peer review:
https://github.com/DennievanderStarre/PRG01-8/issues/1

#### online game:
https://socialcollab.nl/TypeScriptGame/

