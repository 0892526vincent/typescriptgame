const gulp = require('gulp'),
  gutil = require('gulp-util'),
  sass = require('gulp-sass'),
  connect = require('gulp-connect'),
  uglify = require('gulp-uglify-es').default,
  uglifycss = require('gulp-uglifycss'),
  concat = require('gulp-concat'),
  ts = require('gulp-typescript'),
  cleanCSS = require('gulp-clean-css'),
  imagemin = require('gulp-imagemin');


let tsSources = ['scripts/**/*.ts'],
  sassSources = ['styles/*main.scss'],
  htmlSources = ['index.html'],
  imgSources = ['images/*'],
  outputDir = 'assets',
  tsProject = ts.createProject('tsconfig.json');

gulp.task('sass', function () {
  gulp.src(sassSources)
    .pipe(sass({
      style: 'expanded'
    }))
    .on('error', gutil.log)
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(concat('style-min.css'))
    .pipe(gulp.dest(outputDir))
    .pipe(connect.reload())
});

gulp.task('scripts', function () {
  let tsResult = gulp.src(tsSources) // or tsProject.src()
    .pipe(tsProject());

  return tsResult.js.pipe(gulp.dest(outputDir));
});

gulp.task('image', function () {
  gulp.src(imgSources)
    .pipe(imagemin())
    .pipe(gulp.dest(outputDir + '/images/'));
});

gulp.task('watch', function () {
  gulp.watch(tsSources, ['scripts']);
  gulp.watch('styles/*.scss', ['sass']);
  gulp.watch(htmlSources, ['html']);
  gulp.watch(imgSources, ['image']);

});

gulp.task('connect', function () {
  connect.server({
    root: '.',
    livereload: true,
    port: 8009
  })
});

gulp.task('html', function () {
  gulp.src(htmlSources)
    .pipe(connect.reload())
});

gulp.task('default', ['html', 'scripts', 'sass', 'image', 'connect', 'watch']);
